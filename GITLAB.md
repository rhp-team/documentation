## What is Gitlab?
GitLab is a complete DevOps platform, delivered as a single application. This makes GitLab unique and makes Concurrent DevOps possible, unlocking your organization from the constraints of a pieced together toolchain. Join us for a live Q&A to learn how GitLab can give you unmatched visibility and higher levels of efficiency in a single application across the DevOps lifecycle.

## How to delete a previously set password on git?
```
git config --system --unset credential.helper
```

## How to login to Gitlab using the command line?
Do not use your GitLab password, but create an access token and use it instead of your password:

1. In GitLab, go to Settings > Access Tokens
2. Create a new token (check api)
git clone ...
3. When you are asked for your password, copy and paste the access token instead of your GitLab password